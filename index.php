<?php

class DHL24_webapi_client extends SoapClient
{
    const WSDL = 'https://sandbox.dhl24.com.pl/webapi2';
    public function __construct()
    {
        parent::__construct( self::WSDL );
    }
}
$authData = [
    "username" => 'FLIXAPPLE_TEST',
    "password" => 'LOlG@Ym*7iuUr*G',
];

function createShipment($authData) {
    
    // createShipments()
    $shipper = [
        "name"=>"Jan Kowalski", //required
        "postalCode"=>"52437",//required
        "city"=> "Wroclaw",//required
        "street"=> "Balzaka",//required
        "houseNumber"=> "32",//required
        "apartmentNumber" => "6",//optional
        "contactPhone"=> "123123123",//optional
        "contactEmail" => "asd@asd.pl"//optional
        ];
    $receiver = [
        "addresType" => "B",//required
        "country" => "PL",//required
        "name"=>"Pawel Nowak",//required
        "postalCode"=>"54616",//required
        "city"=> "Wrocław",//required
        "street"=> "Balzaka",//required
        "houseNumber"=> "32/8",//required
        "apartmentNumber" => "32",//optional
        "contactPhone"=> "123123123",//optional
        ];
    $pieceList = [
        'type' => 'PACKAGE',//required ENVELOPE, PACKAGE, PALLET
        'width' => 80,//required
        'height' => 40,//required
        'length' => 40,//required
        'weight' => 15,//required
        'quantity' => 1,//required
        'nonStandard' => false//optional
        ];
    $payment = [
        "paymentMethod" =>"BANK_TRANSFER",//required
        "payerType"=> "SHIPPER",//required
        "accountNumber" => "6000000",//required
    ];
    $service = [
        'product' => 'AH',//required AH - przesyłka krajowa
    ];
    $shipmentDate = "2021-09-21";//required
    $content = 'Zawartość przesyłki';//required zawartość przesyłki string(30)
    $comment = 'Dodatkowy komentarz (widoczny na liście przewozowym)';//optional string(100)
    
    
    
    $data['item'] = [
        'shipper' => $shipper,
        'receiver' => $receiver,
        'pieceList' => [
            'item' => $pieceList,
        ],
        'payment' => $payment,
        'service' => $service,
        'shipmentDate' => $shipmentDate,
        'content' => $content,
        'skipRestrictionCheck' => true //optional Czy pominąć sprawdzenie restrykcji
    ];
    
    $params = [
        'authData' => $authData,
        'shipments' => $data,
    ];
     
    $client = new DHL24_webapi_client;
    
    $result = $client->createShipments($params);
    
    
    $id = $result->createShipmentsResult->item->shipmentId;
    echo "ID stworzonej przesyłki: " . $id; 
    ?>
    </br>
    <?php
    return bookCourier($client, $authData, $id);
}


function bookCourier($client, $authData, $id) {
    // BOOK COURIER()
    $bookCourierParams = [
        'authData' => $authData,//required
        'pickupDate' => '2021-09-21',//required
        'pickupTimeFrom' => '10:00',//required
        'pickupTimeTo' => '20:00',//required
        'shipmentIdList' => [$id],//required
        "courierWithLabel" => true,//optional Czy kurier ma przyjechać z etykietą
        "additionalInfo" => "Dodatkowe informacje dla kuriera" //optional Dodatkowe informacje dla kuriera string(50)
    ];

    $result2 = $client->bookCourier($bookCourierParams);
    $orderId = $result2->bookCourierResult->item;
    echo "ID zamówienia (kuriera): " . $orderId;
}

createShipment($authData);



?>